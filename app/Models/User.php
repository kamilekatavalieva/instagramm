<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * @return HasMany
     */
    public function photos():HasMany
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * @return BelongsToMany
     */
    public function followings():BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follower_user', 'follower_id',
            'user_id');
    }


    /**
     * @return BelongsToMany
     */
    public function followers():BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follower_user', 'user_id',
            'follower_id');
    }


    /**
     * @return HasMany
     */
    public function likes():HasMany
    {
        return $this->hasMany(Like::class);
    }

}
