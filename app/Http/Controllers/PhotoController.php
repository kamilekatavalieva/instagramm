<?php

namespace App\Http\Controllers;

use App\Http\Form\PhotoActionsForm;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhotoController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::all();
        return view('photos.index', compact('photos'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $users = User::all();
        return view('photos.create', compact('users'));
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request): RedirectResponse
    {
        $photo = new Photo();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $path = $file->store('photos', 'public');
            $photo->photo = $path;
        }
        $photo->user_id = $request->user()->id;
        $photo->save();
        return redirect()->route('users.show');
    }



    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {
        return view('photos.show', compact('photo'));
    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function edit(Photo $photo)
    {
        $users = User::all();
        return view('photos.edit', compact('photo', 'users'));

    }


    /**
     * @param PhotoRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function update(PhotoRequest $request, Photo $photo):RedirectResponse
    {
        $photo = PhotoActionsForm::execute($request,$photo);
        return redirect()->route('photos.show', ['photo'=>$photo])
            ->with('status', "Photo  updated successfully!");
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo):RedirectResponse
    {
        $photo->delete();
        return redirect()->route('photos.index')->with('status', "Photo  deleted successfully!");
    }
}
