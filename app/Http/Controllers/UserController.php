<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }


    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function subscribe(User $user): RedirectResponse
    {
        Auth::user()->followings()->attach($user);
        return redirect()->route('photos.index');
    }


    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function unsubscribe(User $user): RedirectResponse
    {
        Auth::user()->followings()->detach($user);
        return redirect()->route('photos.index');
    }
}
