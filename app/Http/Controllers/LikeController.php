<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request):RedirectResponse
    {
        $like = new Like();
        $like->user()->associate($request->user());

        $like->photo_id = $request->photo;

        $like->save();

        return redirect()->back();

    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo):RedirectResponse
    {
        foreach($photo->likes as $like) {
            if ($like->photo_id == $photo->id and $like->user_id == Auth::user()->id)
            {
                $like->delete();
            }
        }
        return redirect()->back();
    }
}
