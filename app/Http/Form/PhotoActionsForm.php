<?php

namespace App\Http\Form;

use App\Models\Photo;
use Illuminate\Http\Request;

class PhotoActionsForm extends PhotoForm
{

    /**
     * @inheritDoc
     */
    protected function handle(Request $request, Photo $photo)
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $photo->update($data);
        return $photo;
    }
}
