<?php

namespace App\Http\Form;

use App\Models\Photo;
use Illuminate\Http\Request;

abstract class PhotoForm
{
    /**
     * @param Request $request
     * @return mixed
     */
    public static function execute(Request $request, Photo $photo)
    {
        return(new static)->handle($request, $photo);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected abstract function handle(Request $request, Photo $photo);
}
