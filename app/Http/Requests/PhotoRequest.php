<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'photo' => ['required','image'],
            'user_id' => ['required']
        ];
    }

    public function messages(): array
    {
        return [
            'photo.required' => "Заполните поле :attribute",
            'photo.image' => ":Attribute Вы можете загрузить файл только с данным расширением: jpg, jpeg, png, bmp, gif, svg, webp ",
        ];
    }
}
