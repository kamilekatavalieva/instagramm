<?php

namespace Database\Seeders;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(10)->has(
                Photo::factory()->count(20))
            ->create();

        for($i = 0; $i < 10; $i++) {
            $user = User::find(rand(1, 10));
            $user->followers()->attach(User::find(rand(1, 10)));
        }
    }

}
